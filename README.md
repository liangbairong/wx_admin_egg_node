# init
数据库文件夹
./db

# 配置
数据库名：wx


### 开发

```bash
$ npm i
$ npm run debug  //默认开发环境 才能热更新 EGG_SERVER_ENV 会影响热更新插件
$ npm run server:dev  //开发环境
$ npm run server:test  //测试环境
$ npm run server:prod  //生产环境
$ open http://localhost:7001/
```

### 服务器部署

```bash
$ npm run start:dev  //开发环境
$ npm run start:test  //测试环境
$ npm run start:prod  //生产环境

$ npm stop  //停止服务
```