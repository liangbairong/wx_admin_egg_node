
'use strict';

module.exports = appInfo => {
  const config = exports = {};
  config.keys = appInfo.name + '_1568685835614_3976';
  // 全局常量
  config.CONST = {
    ROOT: '',
    UPLOAD_URL: 'http://127.0.0.1:7001',
    BOOK_SOURCE_MAP: {
      1: {
        url: 'https://www.qidian.com',
        name: '起点',
      },
    },
  };
  const userConfig = {
    // myAppName: 'egg',
  };
  config.cluster = {
    listen: {
      port: 7001,
      hostname: '0.0.0.0', // 不建议设置 hostname 为 '0.0.0.0'，它将允许来自外部网络和来源的连接，请在知晓风险的情况下使用
      // path: '/var/run/egg.sock',
    },
  };
  config.mysql = {
    // 单数据库信息配置
    client: {
      // host
      host: '127.0.0.1',
      // 端口号
      port: '3306',
      // 用户名
      user: 'root',
      // 密码
      password: '1234567890',
      // 数据库名
      database: 'wx',
    },
    // 是否加载到 app 上，默认开启
    app: true,
    // 是否加载到 agent 上，默认关闭
    agent: false,
  };

  // 报错处理
  config.onerror = {
    errorPageUrl: (err, ctx) => ctx.errorPageUrl || '/500',
    json: (err, ctx) => {
      ctx.body = {
        code: err.status,
        msg: err.message,
      };
    },
  };
  // 自定义中间件
  config.middleware = [ 'httpError', 'verLogin' ];
  config.httpError = {
    match: '/',
  };
  config.verLogin = {
    match: '/token',
  };
  // 跨域配置
  config.cors = {
    origin: [ '*' ],
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
    credentials: true,
  };
  config.security = {
    // csrf: false,
    csrf: {
      enable: false, // 前后端分离，post请求不方便携带_csrf
      ignoreJSON: true,
      headerName: 'authorization',
    },
    methodnoallow: {
      enable: false,
    },

  };
  // 上传文件
  config.multipart = {
    mode: 'stream',
  };


  return {
    ...config,
    ...userConfig,
  };
};

